﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneManager : MonoBehaviour {
    AppManager appManager;


	void Start () {
        appManager = AppManager.Instance;
	}

    public void OpenMenu(string s) {
        appManager.OpenMenu(s);
    }

}
