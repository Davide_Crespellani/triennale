﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class AppManager : Singleton<AppManager> {
    public static Action<int> newStage;
    public int progress;

    new void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    void Start() {
        //setta il progress leggendo la player pref o, nel caso ci si trovi al primo avvio, lo setta a 0
        progress = 0;
        progress = PlayerPrefs.GetInt("Progress") > progress? PlayerPrefs.GetInt("Progress"):0;
        
    }

    public void LoadSceneOnTop(string s) {
        //carica in modo additivo la scena, controllando prima che non sia già presente, se lo è, semplicemente la rende attiva.
        Scene sceneOnTop = SceneManager.GetSceneByName(s);
        if (sceneOnTop.name == null) {
            SceneManager.LoadScene(s, LoadSceneMode.Additive);
        }
        else
            sceneOnTop.GetRootGameObjects()[0].SetActive(true);
    }

    public void LoadScene(string s) {
        //carica in modo additivo la scena 
        SceneManager.LoadScene(s, LoadSceneMode.Single);
    }

    public void StageClear() {
        //chiama increaseProgress, chiama LoadSceneOnTop selezionando scena in base a progress e lancia l'evento newStage
        IncreaseProgress();
        LoadScene("Stage" + progress);
        if (newStage!=null)
            newStage.Invoke(progress);

    }

    public void OpenMenu(string menu) {
        LoadSceneOnTop(menu);
    }

    public void IncreaseProgress() {
        //incrementa progress e salva il progress attuale nella playerPref

        progress++;
        PlayerPrefs.SetInt("Progress", progress);

    }

    public void ResetProgress() {
        // resetta il progress e  salva il progress attuale nella playerPref
    }


}
